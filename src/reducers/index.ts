import { actionTypes } from '../actions/action-types';
import { AppState } from '../types/app-state';

const initialState: AppState = new AppState();

type action = {type: actionTypes, payload: any};

export default (state: AppState = initialState, action: action): AppState => {
    switch(action.type) {
    case (actionTypes.setContacts):
        return {
            ...state,
            contacts: action.payload,
        };
    default:
        return state;
    }
};
