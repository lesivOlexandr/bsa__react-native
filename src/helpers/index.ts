import callByPhone from 'react-native-phone-call';

export default function call(phoneNumber: string): void {
    callByPhone({ number: phoneNumber, prompt: true });
}