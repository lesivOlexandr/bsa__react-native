import { actionTypes } from '../actions/action-types';
import { contactsService } from '../services/contacts-service';
import { call, put, all, takeEvery, fork } from 'redux-saga/effects';
import { Contact } from 'expo-contacts';
import { Action } from '../types/action';

function* getContacts() {
    try {
        const contacts: Contact[] = yield call(contactsService.getAll);
        yield put({ type: actionTypes.setContacts, payload: contacts });
    } catch (error) {
        alert(error.message);
    }
}

export function* watchGetContacts() {
    yield takeEvery(actionTypes.getContacts, getContacts);
}

function* deleteContact(action: Action<string>) {
    try {
        yield call(contactsService.deleteOne, action.payload);
        put({ type: actionTypes.getContacts });
    } catch (error) {
        alert(error.message);
    }
}

export function* watchDeleteContact() {
    yield takeEvery(actionTypes.deleteContact, deleteContact);
}

export default function* ContactsSagas() {
    yield all([
        fork(watchGetContacts),
        fork(watchDeleteContact)
    ]);
}
