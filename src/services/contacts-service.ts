import * as Contacts from 'expo-contacts';
import * as Permissions from 'expo-permissions';
import { PermissionsAndroid } from 'react-native';

export class ContactsService {
    public async getAll(): Promise<Contacts.Contact[]>{
        const { status } = await Contacts.requestPermissionsAsync();
        if (status === 'denied') {
            throw new Error('Permission to get contacts denied');
        }
        const response: Contacts.ContactResponse = await Contacts.getContactsAsync();
        return response.data;
    }

    public async deleteOne(id: string) {
        console.log(id);
        const { status } = await Permissions.getAsync(Permissions.CONTACTS);
        console.log(status);
        if (status !== 'granted') {
            throw new Error('Permission to update contacts denied');
        }
        return Contacts.removeContactAsync(id);
    }

    public async updateOne(id: string) {
        // console.log(id);
        // const status = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS);
    }
}

export const contactsService = new ContactsService;
