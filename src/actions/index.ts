import { actionTypes } from './action-types';
import { Contact } from 'expo-contacts';

const makeActionHandler = <T>(actionType: actionTypes) => {
    return (data?: T) => ({
        type: actionType,
        payload: data
    });
};
  
export const getContacts = makeActionHandler<Contact[]>(actionTypes.getContacts);
export const deleteContact = makeActionHandler<string>(actionTypes.deleteContact);
