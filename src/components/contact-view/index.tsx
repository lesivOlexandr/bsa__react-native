import React, { useState } from 'react';
import { View, Text, Keyboard, Image } from 'react-native';
import { Input, Item, Label, Button, Icon } from 'native-base';
import styles from './styles';
import { RouteProp } from '@react-navigation/native';
import { Contact, PhoneNumber } from 'expo-contacts';
import call from '../../helpers';
import { deleteContact } from '../../actions';
import { connect } from 'react-redux';

interface Props {
    navigation: any;
    route: RouteProp<Record<'Home', Contact>, 'Home'>;
    deleteContact: (id: string) => void;
}

const ContactView = (props: Props): JSX.Element => {
    const contact: Contact = props.route.params;
    const primaryNumber: string = findPrimaryNumber(contact.phoneNumbers);
    const [phoneNumber, setPhoneNumber] = useState<string>(primaryNumber);
    const [name, setName] = useState<string>(contact.name);
    const onButtonPress = () => {
        Keyboard.dismiss();
    };

    const onCall = () => {
        onButtonPress();
        call(phoneNumber);
    };

    const onDelete = () => {
        onButtonPress();
        props.deleteContact(contact.id);
        // props.navigation.navigate('Home');
    };

    const onPhoneNumberInput = (text: string) => {
        const validText = text.replace(/[^+0-9]/g, '');
        setPhoneNumber(validText);
    };

    const onNameInput = (text: string) => {
        setName(text);
    };

    function findPrimaryNumber(phoneNumbers?: PhoneNumber[]): string {
        if (!phoneNumbers?.length) {
            return '';
        }
        const primary: PhoneNumber = phoneNumbers.reduce((acc, curr) => curr.isPrimary ? curr : acc);
        return primary.number || '';
    }

    return (
        <View>
            <View style={styles.imageContainer}>
                <Image 
                    source={
                        contact.imageAvailable ?
                            contact.image :
                            require('../../../assets/default-contact-img.png')
                    }    
                    style={styles.image}
                />
            </View>
            <View style={styles.form}>
                <Item floatingLabel style={styles.inputContainer}>
                    <Label style={styles.label}>Name</Label>
                    <Input
                        value={name}
                        textContentType="name"
                        onChangeText={onNameInput}
                    />
                </Item>
                <Item floatingLabel style={styles.inputContainer}>
                    <Label style={styles.label}>Phone Number</Label>
                    <Input
                        value={phoneNumber}
                        textContentType="telephoneNumber"
                        keyboardType="phone-pad"
                        onChangeText={onPhoneNumberInput}
                    />
                </Item>
            </View>
            <View style={styles.actions}>
                <Button 
                    style={[styles.button, styles.callButton]}
                    onPress={onCall} 
                    small
                >
                    <Icon name="md-call" style={styles.icon}/>
                    <Text>Call</Text>
                </Button>
                <Button 
                    style={[styles.button, styles.deleteButton]}
                    onPress={onDelete}
                    small 
                >
                    <Text>Delete</Text>
                    <Icon type="MaterialIcons" name="clear" style={styles.icon} />
                </Button>
            </View>
        </View>
    );
};

const mapDispatchToProps = {
    deleteContact
};

export default  connect(null, mapDispatchToProps)(ContactView);
