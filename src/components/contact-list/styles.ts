import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
    },
    contactsContainer: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        marginTop: 5
    },
    searchBarWrapper: {
        borderBottomWidth: 0
    },
    searchBar: {
        marginHorizontal: 30,
        marginTop: 20,
        paddingLeft: 15,
        borderWidth: 1,
        borderRadius: 50,
        borderColor: '#333'
    },
    contactsList: {
        alignSelf: 'stretch',
        marginTop: 25
    }
});

export default styles;