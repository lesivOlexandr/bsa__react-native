import React, { useEffect } from 'react';
import { View, TouchableWithoutFeedback, Keyboard, FlatList } from 'react-native';
import { Container, Item, Icon, Input } from 'native-base';
import ContactListItem from '../contact-list-item';
import styles from './styles';
import { Contact } from 'expo-contacts';
import { AppState } from '../../types/app-state';
import { connect } from 'react-redux';
import { getContacts } from '../../actions';

interface Props {
    navigation: any;
    contacts: Contact[];
    getContacts: () => void;
}

export const ContactList: React.FC<Props> = (props): JSX.Element => {
    const { navigation, contacts } = props;
    useEffect(() => {
        props.getContacts();
    }, []);
    return (
        <Container style={styles.container}>
            <View style={styles.searchBar}>
                <Item style={styles.searchBarWrapper}>
                    <Icon name="ios-search" />
                    <Input placeholder="Search" />
                </Item>
            </View>
            <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()} accessible={false}>
                <View style={styles.contactsContainer}>
                    <FlatList
                        style={styles.contactsList}
                        data={contacts}
                        renderItem={({item}) => <ContactListItem item={item} navigation={navigation} />}
                        keyExtractor={(item) => item.id}
                    />
                </View>
            </TouchableWithoutFeedback>
        </Container>
    );
};

// class ContactList extends React.Component<Props, State> {
//     constructor(props: Props){
//         super(props);
//         const contacts: Contact[] = [];
//         this.state = { contacts };
//     }

//     componentDidMount(): void {
//         try {

//             const contacts: Contact[] = await contactsService.getAll();
//             this.setState({ contacts });
//         } catch(e) {
//             alert(e.message);
//         }
//     }

    
// }

const mapStateToProps = (state: AppState) => ({
    contacts: state.contacts
});

const mapDispatchToProps = {
    getContacts
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactList);
