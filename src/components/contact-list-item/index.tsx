import React from 'react';
import { View, Text, TouchableNativeFeedback, Keyboard } from 'react-native';
import { Icon } from 'native-base';
import styles from './styles';
import { Contact, Email } from 'expo-contacts';

interface Props {
    item: Contact;
    navigation: any;
}

class ContactListItem extends React.PureComponent<Props> {
    render() {
        const { item, navigation } = this.props;
        const onPress = () => {
            Keyboard.dismiss();
            navigation.navigate('Contact', item);
        };
    
        const findPrimaryEmail = (emails?: Email[]): string | null => {
            if (!emails?.length) {
                return null;
            }
            const primary: Email = emails.reduce((acc, curr) => curr.isPrimary ? curr: acc);
            return primary.email || null;
        };
    
        const primaryEmail: string | null = findPrimaryEmail(item.emails);
    
        return (
            <TouchableNativeFeedback onPress={onPress} background={TouchableNativeFeedback.Ripple('#aaa', false)}>
                <View style={styles.container}>
                    <View style={styles.contact}>
                        <Icon type="MaterialIcons" name="perm-contact-calendar" />
                        <View style={styles.contactDetail}>
                            <Text style={styles.contactName}>{item.name}</Text>
                            {primaryEmail && 
                                <Text style={styles.contactEmail} >{primaryEmail}</Text>
                            }
                        </View>
                    </View>
                    <Icon name="md-information-circle-outline"></Icon>
                </View>
            </TouchableNativeFeedback>
        );
    }
}

export default ContactListItem;