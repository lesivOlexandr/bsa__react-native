import { Contact } from 'expo-contacts';

export class AppState {
    constructor(
        public contacts: Contact[] = [],
        public selectedContact: Contact | null = null
    ){}
}
